
# react-native-esp-communicate

## Getting started

`$ npm install https://tructmkst@bitbucket.org/tructmkst/espcommunicate.git --save`

### Mostly automatic installation

`$ react-native link react-native-esp-communicate`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-esp-communicate` and add `RNEspCommunicate.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNEspCommunicate.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNEspCommunicatePackage;` to the imports at the top of the file
  - Add `new RNEspCommunicatePackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-esp-communicate'
  	project(':react-native-esp-communicate').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-esp-communicate/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-esp-communicate')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNEspCommunicate.sln` in `node_modules/react-native-esp-communicate/windows/RNEspCommunicate.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Esp.Communicate.RNEspCommunicate;` to the usings at the top of the file
  - Add `new RNEspCommunicatePackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNEspCommunicate from 'react-native-esp-communicate';

// TODO: What to do with the module?
RNEspCommunicate;

- Get wifi info
RNEspCommunicate.fetchNetInfo().then((wifiInfos) =>
{
  console.log(wifiInfos.SSID);
  console.log(wifiInfos.BSSID);

});

- Connect to wifi
RNEspCommunicate.executeConnect(ssid, bssid, password).then((status) =>
{
  // status = true => connect success
  // status = false => connect failed
  console.log(status);
});

- Cancel
RNEspCommunicate.cancel();
```
  