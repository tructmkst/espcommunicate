#import "RNEspCommunicate.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"

@interface EspTouchDelegateImpl : NSObject<ESPTouchDelegate>

@end

@implementation EspTouchDelegateImpl

-(void) onEsptouchResultAddedWithResult: (ESPTouchResult *) result
{
    NSLog(@"EspTouchDelegateImpl onEsptouchResultAddedWithResult bssid: %@", result.bssid);
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [self showAlertWithResult:result];
    });
}
@end

@interface RNEspCommunicate ()

// to cancel ESPTouchTask when
@property (atomic, strong) ESPTouchTask *_esptouchTask;

// the state of the confirm/cancel button
@property (nonatomic, assign) BOOL _isConfirmState;

// without the condition, if the user tap confirm/cancel quickly enough,
// the bug will arise. the reason is follows:
// 0. task is starting created, but not finished
// 1. the task is cancel for the task hasn't been created, it do nothing
// 2. task is created
// 3. Oops, the task should be cancelled, but it is running
@property (nonatomic, strong) NSCondition *_condition;
@property (nonatomic, strong) EspTouchDelegateImpl *_esptouchDelegate;


// @property (nonatomic, assign) RCTPromiseResolveBlock _resolvePromise;
// @property (nonatomic, assign) RCTPromiseRejectBlock _rejectPromise;


//- (void) registerBoardInfos;

@end


@implementation RNEspCommunicate

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(fetchNetInfo: (RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
    
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    
    NSDictionary *SSIDInfo = NULL;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(
                                     CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    if (SSIDInfo != NULL){
        NSDictionary *result = [[NSDictionary alloc] initWithObjectsAndKeys:[SSIDInfo objectForKey:@"BSSID"], @"bssid", [SSIDInfo objectForKey:@"SSID"], @"ssid", nil];
        resolve(result);
    } else {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"errorConnection" forKey:NSLocalizedDescriptionKey];
        // populate the error object with the details
        NSError *error = [NSError errorWithDomain:@"wifi" code:401 userInfo:details];
        //failed
        reject(@"401", @"errorConnection", error);
    }
}

RCT_EXPORT_METHOD(executeConnect: (NSString *)ssid bssid:(NSString *)bssid password:(NSString *)password resolve:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
    self._condition = [[NSCondition alloc]init];
    self._esptouchDelegate = [[EspTouchDelegateImpl alloc]init];
    
    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSLog(@"ESPViewController do the execute work...");
        // execute the task
        ESPTouchResult *firstResult = [self executeForResult:ssid bssid:bssid password:password];
        // show the result to the user in UI Main Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!firstResult.isCancelled && [firstResult isSuc])
            {
                //success
                NSNumber *result = [NSNumber numberWithBool:YES];
                resolve(result);
            } else {
                //failed
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"canNotConnectToBoard" forKey:NSLocalizedDescriptionKey];
                // populate the error object with the details
                NSError *error = [NSError errorWithDomain:@"world" code:401 userInfo:details];
                                //failed
                reject(@"401", @"canNotConnectToBoard", error);
            }
        });
    });
}

RCT_EXPORT_METHOD(cancel)
{
    [self._condition lock];
    if (self._esptouchTask != nil)
    {
        [self._esptouchTask interrupt];
    }
    [self._condition unlock];
}
#pragma mark - the example of how to use executeForResult

- (ESPTouchResult *) executeForResult:(NSString *)ssid bssid:(NSString *)bssid password:(NSString *)password
{
    [self._condition lock];
    self._esptouchTask =
    [[ESPTouchTask alloc]initWithApSsid:ssid andApBssid:bssid andApPwd:password];
    // set delegate
    [self._esptouchTask setEsptouchDelegate:self._esptouchDelegate];
    [self._condition unlock];
    ESPTouchResult * esptouchResult = [self._esptouchTask executeForResult];
    NSLog(@"ESPViewController executeForResult() result is: %@",esptouchResult);
    return esptouchResult;
}

@end
